CREATE TABLE especialitat_borsa(
    id INT NOT NULL AUTO_INCREMENT,
    data DATE,
    id_st_dept VARCHAR(4),
    id_cos VARCHAR(5),
    id_esp VARCHAR(4),
    PRIMARY KEY (id));