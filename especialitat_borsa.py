from data_converter.output_object import OutputObject

class EspecialitatBorsa(OutputObject):
    def __init__(self):
        self.data = ""
        self.id_st_dept = ""
        self.id_cos = ""
        self.id_espe = ""

    def __str__(self):
        return "[data: " + self.data + \
               ", id_st_dept: " + self.id_st_dept + \
               ", id_cos: " + self.id_cos + \
               ", id_espe: " + self.id_espe + \
               "]"