import argparse
import os

from especialitat_borsa_converter import EspecialitatBorsaConverter

parser = argparse.ArgumentParser()
# -l: do not download data. use local data already present
parser.add_argument("-l", "--localdata", action='store_true')
args = parser.parse_args()

c = EspecialitatBorsaConverter(os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'config'))

if not args.localdata:
    print("Obtenint dades")
    c.get_data()
print("Convertint dades a un format llegible")
c.convert()
print("Extraient dades")
c.parse()
print("Exportant a la base de dades")
c.export()
print("Fi")