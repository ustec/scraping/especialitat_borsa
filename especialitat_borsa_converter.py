import datetime
import logging
from lxml import html
import os
from pyvirtualdisplay import Display
import re
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from sqlalchemy import Table
from sqlalchemy.orm import mapper
import time

from config import ConfigEspecialitatBorsa
from data_converter.data_converter import DataConverter
from especialitat_borsa import EspecialitatBorsa

# TODO: Aquesta app s'executa a la bbdd departament_augusta a neo ... cal integrar-la a la bbdd departament.
# Problemes: hi ha taules amb el mateix nom procedent de augusta que "xoquen" amb les que estem fent servir a neo. 
# Cal reanomenar o unificar taules, si cal repetitnt camps.
# Cal ampliar taula sstt nous. 
# 
# Aquesta aplicació esta associada a programa PHP que genera la taula que també cal revisar

class EspecialitatBorsaConverter(DataConverter):

    def __init__(self, config_file_path):
        self.config = ConfigEspecialitatBorsa(config_file_path)
        super(EspecialitatBorsaConverter, self).__init__()
        mapper(EspecialitatBorsa, self.table)
        logging.basicConfig(filename=self.config.log_file, level=logging.WARNING)

    def get_input_files(self):
        # Get all serveis territorials codes
        sts = self.get_sts()
        # Get all cos codes
        cossos = self.get_cossos()
        # Get form url
        form_url = open(self.config.urls).read().splitlines()[0]
        # Start Firefox
        Display(visible=1, size=(800, 600)).start()
        driver = webdriver.Firefox()
        driver.get(form_url)
        for st in sts:
            for cos in cossos:
                select = Select(driver.find_element_by_id('P500_AATT'))
                select.select_by_value(st)
                select = Select(driver.find_element_by_id('P500_COSSOS'))
                try:
                    select.select_by_value(cos + 'EC')
                except NoSuchElementException:
                    continue
                submit_button = driver.find_element_by_name("P500_CERCA")
                submit_button.click()
                time.sleep(2)
                with open(os.path.join(self.config.raw_data_dir, st + "_" + cos + ".html"),"w") as f:
                    f.write(driver.page_source)
        driver.quit()

    def get_sts(self):
        # taula amb els serveis territorials (ampliar amb els nous SSTT )
        st_table = Table("servei_territorial", self.metadata, autoload=True)
        q = self.session.query(st_table.c.id_st_dept)
        sts = []
        for st in q:
            sts.append(st[0])
        return sts

    def get_cossos(self):
        st_table = Table("cos", self.metadata, autoload=True)
        # Get all cos codes except 999 (internal code)
        q = self.session.query(st_table.c.id_cos).filter(st_table.c.id_cos!='999')
        cossos = []
        for cos in q:
            cossos.append(cos[0])
        return cossos

    def convert(self):
        pass

    def get_valid_records(self):
        # Get records
        records = []
        today = datetime.date.today()
        # today = datetime.date(2022,1,21)
        # Loop files with readable data
        for filename in os.listdir(self.config.raw_data_dir):
            st = filename.split("_")[0]
            cos = filename.split("_")[1].split(".")[0]
            # Get html content of eahc file
            html_page = open(os.path.join(self.config.raw_data_dir, filename)).read()
            tree = html.fromstring(html_page)
            
            esps_raw = tree.xpath("//tr[@class='highlight-row']/td/text()")[1:]
            # esps_raw = tree.xpath("//table[@id = 'report_R33381509755664239']/tbody/tr[3]/td/text()")[1:]
            for esp in esps_raw:
                esp_code_re = re.match(r'.*\((.*)\).*', esp)
                if(esp_code_re):
                    esp_code = esp_code_re.group(1)
                    record = []
                    record.append(today)
                    record.append(st)
                    record.append(cos)
                    record.append(esp_code)
                    records.append(record)
        return records

    def build_objects(self, raw_records):
        # Remove records from database since they may have been changed
        # and we must have the last ones
        today = datetime.date.today()
        conn = self.eng.connect()
        esp_query = self.table.delete().where(self.table.c.data == today)
        conn.execute(esp_query)
        # Loop all records
        for r in raw_records:
            eb = EspecialitatBorsa()
            eb.data = r[0]
            eb.id_st_dept = r[1]
            eb.id_cos = r[2]
            eb.id_espe = r[3]
            self.objects.append(eb)
